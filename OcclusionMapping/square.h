#ifndef SQUARE_H
#define SQUARE_H

int numSquareVerts = 4;

float SquareVerts [][3] = {
	{-1.0f, -1.0f, 0.0f},
	{1.0f, -1.0f, 0.0f},
	{1.0f,  1.0f, 0.0f},
	{-1.0f,  1.0f, 0.0f}
 };

int numSquareFaces = 2;

unsigned int SquareFaces [][3] = {
     {0, 1, 2,},
     {0, 2, 3},
     
};

float SquareVertNorms [][3] = {
	{0.0f, 0.0f, 1.0f},
	{0.0f, 0.0f, 1.0f},
	{0.0f, 0.0f, 1.0f},
	{0.0f, 0.0f, 1.0f}
};

float SquareVertTangents [][3] = {
	{1.0f, 0.0f, 0.0f},
	{1.0f, 0.0f, 0.0f},
	{1.0f, 0.0f, 0.0f},
	{1.0f, 0.0f, 0.0f}
};

float SquareVertTex[][2] = {
	{0.0f,1.0f},
	{1.0f,1.0f},
	{1.0f,0.0f},
	{0.0f,0.0f}
	};
#endif
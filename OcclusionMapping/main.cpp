#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <GL\glew.h>
#include <fstream>
#include <sstream>
#include <string>

#include <cstdlib>
#include <ctime>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "square.h"
#include "phong.h"
#include "LabelClass.h"

#define ATTRIB_POSITION 0
#define ATTRIB_COLOR 1
#define ATTRIB_NORMAL 2
#define ATTRIB_TEXTURE_COORDINATE 3
#define ATTRIB_TANGENT 4

#define ROTATION_SPEED 1.0f

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

using namespace std;

LabelClass timeLabel, heightMapScalLabel, minSamplesLabel, maxSamplesLabel;

GLuint vao, vbo[5]; // Handles for a VAO and two VBOs

GLuint *texID;

TTF_Font* textFont;

clock_t currentTime, lastTime;

/////////////////
//DEMO Variables
/////////////////
//these variables are used to change the parametters for the demo

GLuint shaderprogramTextureSimple, shaderprogramPOM; // handle for shader program

bool POM = true;//variable to know if we use parralax Occlusion Mapping

float heightMapScal = 0.08f;//variable for changing the intensity of the effect; initial value : 0.04
int minSamples= 4;// variable for the quality of the effect decrease for better quality; initial value: 4
int maxSamples= 40;// variable for the quality of the effect increasefor better quality; initial value : 40

float rotation_x = 0;//angle in degree rotating around the y axis (rotate in the x direction)
float rotation_y = 0;//angle in degree rotating around the x axis (rotate in the y direction)

float rotationDirection_x;//speed of the rotation in x direction(take the value of ROTATION_SPEED or -ROTATION_SPEED)
float rotationDirection_y;//speed of the rotation in y direction(take the value of ROTATION_SPEED or -ROTATION_SPEED)

//check if a control key is down
bool rshift = false;
bool lshift = false;

//variables forloading and choosing textures and heightMap
int indexCurrentTexture = 0;
int indexCurrentHeightMap = 0;

//number of different texture and heightmap
int numberTexture = 2;
int numberHeightMap = 3;

char *textureNames[] ={"wall_base.bmp","studdedmetal.bmp"};
char *HeightMapNames[] ={"wall_height.bmp","studdedmetal_height.bmp","heightMap2.bmp"};

void exitFatalError(char *message)
{
    cout << message << " " << endl;
    cout << SDL_GetError();
    SDL_Quit();
	cin.get();
    exit(1);
}

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context)
{
    SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // If you request a context not supported by your drivers,
    // no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
 
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on

    // Turn on x4 multisampling anti-aliasing (MSAA)
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("OpenGL Parallax Occlusion Mapping Demo",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
             800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    // Create opengl context and attach to window
    context = SDL_GL_CreateContext(window);
    // set swap buffers to sync with monitor's vertical refresh rate

    SDL_GL_SetSwapInterval(1); 

    // return the SDL_Window *
    return window;
}

bool handleSDLEvent(SDL_Event const &sdlEvent)
{
	if (sdlEvent.type == SDL_QUIT)
		return false;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		// Can extend this to handle a wider range of keys
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_ESCAPE:
				return false;
			case SDLK_UP:
				rotationDirection_y  = ROTATION_SPEED;
				break;
			case SDLK_DOWN:
				rotationDirection_y  = -ROTATION_SPEED;
				break;
			case SDLK_LEFT:
				rotationDirection_x  = ROTATION_SPEED;
				break;
			case SDLK_RIGHT:
				rotationDirection_x  = -ROTATION_SPEED;
				break;
			case SDLK_p:
				POM = !POM;
				break;
			case SDLK_a:
				if(rshift || lshift)
				{
					heightMapScal -= 0.01f;
					std::stringstream strStream;
					strStream << "height scale : " << heightMapScal;
					heightMapScalLabel.textToTexture(strStream.str().c_str(),textFont);
				}
				else
				{
					heightMapScal += 0.01f;
					std::stringstream strStream;
					strStream << "height scale : " << heightMapScal;
					heightMapScalLabel.textToTexture(strStream.str().c_str(),textFont);
				}
				break;
			case SDLK_s:
				if(rshift || lshift)
				{
					minSamples -= 1;
					std::stringstream strStream;
					strStream << "min Sample : " << minSamples;
					minSamplesLabel.textToTexture(strStream.str().c_str(),textFont);
				}
				else if(minSamples < maxSamples-1)
				{
					minSamples += 1;
					std::stringstream strStream;
					strStream << "min Sample : " << minSamples;
					minSamplesLabel.textToTexture(strStream.str().c_str(),textFont);
				}
				break;
			case SDLK_d:
				if(rshift || lshift && maxSamples > minSamples + 5)
				{
					maxSamples -= 5;
					std::stringstream strStream;
					strStream << "max Sample : " << maxSamples;
					maxSamplesLabel.textToTexture(strStream.str().c_str(),textFont);
				}
				else
				{
					maxSamples += 5;
					std::stringstream strStream;
					strStream << "max Sample : " << maxSamples;
					maxSamplesLabel.textToTexture(strStream.str().c_str(),textFont);
				}
				break;
			case SDLK_RSHIFT:
				rshift = true;
				break;
			case SDLK_LSHIFT:
				lshift = true;
				break;
			case SDLK_r:
				rotation_x = 0;
				rotation_y = 0;
				break;
			case SDLK_t:
				if(indexCurrentTexture < numberTexture-1)
					indexCurrentTexture ++;
				else
					indexCurrentTexture = 0;
				break;
			case SDLK_h:
				if(indexCurrentHeightMap < numberHeightMap-1)
					indexCurrentHeightMap ++;
				else
					indexCurrentHeightMap = 0;
				break;
			default:
				break;
		}
	}
	if (sdlEvent.type == SDL_KEYUP)
	{
		// Can extend this to handle a wider range of keys
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_UP:
				rotationDirection_y  = 0;
				break;
			case SDLK_DOWN:
				rotationDirection_y  = 0;
				break;
			case SDLK_LEFT:
				rotationDirection_x  = 0;
				break;
			case SDLK_RIGHT:
				rotationDirection_x  = 0;
				break;
			case SDLK_RSHIFT:
				rshift = false;
				break;
			case SDLK_LSHIFT:
				lshift = false;
				break;
			default:
				break;
		}
	}
	return true;
}

// loadFile - loads text file into char* fname
// allocates memory - so need to delete after use
char* loadFile(char *fname)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	ifstream file (fname, ios::in|ios::binary|ios::ate);
	if (file.is_open())
	{
		size = (int) file.tellg(); // get file size
		memblock = new char [size+1]; // create buffer w space for null char
		file.seekg (0, ios::beg);
		file.read (memblock, size);
		file.close();
		memblock[size] = 0;
		cout << "file " << fname << " loaded" << endl;
	}
	else
	{
		cout << "Unable to open file " << fname << endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
	}
	return memblock;
}

GLuint loadTexture(char *fname, GLuint *texID)
{
	// generate texture ID
	glGenTextures(1, texID);

	// load file - using core SDL library
	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}
	glActiveTexture(GL_TEXTURE0); 

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, *texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	char *type;
	GLuint colours = tmpSurface->format->BytesPerPixel;
	GLuint format, internalFormat;
	if (colours == 4) // alpha
	{
		internalFormat = GL_RGBA;
		type = "Three colors and alpha";
		if (tmpSurface->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else if (colours == 3)  // no alpha
	{
		internalFormat = GL_RGB;
		type = "Three colors";
		if (tmpSurface->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	else
	{
		format = GL_DEPTH_COMPONENT;
		internalFormat = GL_DEPTH_COMPONENT;
		type = "Depth component";
	}

	glTexImage2D(GL_TEXTURE_2D,0, internalFormat, tmpSurface->w, tmpSurface->h, 0, format, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	// texture loaded, free the temporary buffer

	if(tmpSurface)
	{

		std::cout << "Texture " << fname << " loaded, type : " << type << std::endl;
	}

	SDL_FreeSurface(tmpSurface);

	return *texID;	// return value of texure ID, redundant really
}

// printShaderError
// Display (hopefully) useful error messages if shader fails to compile or link
void printShaderError(GLint shader)
{
  int maxLength = 0;
  int logLength = 0;
  GLchar *logMessage;

  // Find out how long the error message is
  if (!glIsShader(shader))
    glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
  else
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

  if (maxLength > 0) // If message has length > 0
  {
    logMessage = new GLchar[maxLength];
    if (!glIsShader(shader))
       glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
    else
       glGetShaderInfoLog(shader,maxLength, &logLength, logMessage);
    cout << "Shader Info Log:" << endl << logMessage << endl;
    delete [] logMessage;
  }
}

GLuint initShaders(char *vertFile, char *fragFile)
{
	GLuint p, f, v; // Handles for shader program & vertex and fragment shaders

	v = glCreateShader(GL_VERTEX_SHADER); // Create vertex shader handle
	f = glCreateShader(GL_FRAGMENT_SHADER); // " fragment shader handle

	const char *vertSource = loadFile(vertFile); // load vertex shader source
	const char *fragSource = loadFile(fragFile);  // load frag shader source
	
	// Send the shader source to the GPU
	// Strings here are null terminated - a non-zero final parameter can be
	// used to indicate the length of the shader source instead
	glShaderSource(v, 1, &vertSource,0);
	glShaderSource(f, 1, &fragSource,0);
	
	GLint compiled, linked; // return values for checking for compile & link errors

	// compile the vertex shader and test for errors
	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
	cout << "Vertex shader not compiled." << endl;
	printShaderError(v);
	} 

	// compile the fragment shader and test for errors
	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
	cout << "Fragment shader not compiled." << endl;
	printShaderError(f);
	} 
	
	p = glCreateProgram(); 	// create the handle for the shader program
	glAttachShader(p,v); // attach vertex shader to program
	glAttachShader(p,f); // attach fragment shader to program

	glBindAttribLocation(p,ATTRIB_POSITION,"in_Position"); // Bind position to attribute 0
	glBindAttribLocation(p,ATTRIB_COLOR,"in_Color"); // Bind color to attribute 1
	glBindAttribLocation(p,ATTRIB_NORMAL,"in_Normal"); // Bind normal to attribute 2
	glBindAttribLocation(p,ATTRIB_TEXTURE_COORDINATE,"in_TexCoord"); // Bind normal to attribute 3
	glBindAttribLocation(p,ATTRIB_TANGENT,"in_Tangent"); // Bind color to attribute 4
	glLinkProgram(p); // link the shader program and test for errors
	glGetProgramiv(p, GL_LINK_STATUS, &linked);
	if(!linked) {
	cout << "Program not linked." << endl;
	printShaderError(p);
	}

	glUseProgram(p);  // Make the shader program the current active program

	delete [] vertSource; // Don't forget to free allocated memory
	delete [] fragSource; // We allocated this in the loadFile function...

	return p; // Return the shader program handle
}

void draw(SDL_Window *window)
{
	GLuint shaderprogram, newShaderProgram;
	shaderprogram = 0;
	//determinig which shader program is used
	if(POM == true)
	{
		newShaderProgram = shaderprogramPOM;
	}
	else
	{
		newShaderProgram = shaderprogramTextureSimple;
	}

	//test if the shader program is already used.
	if(newShaderProgram != shaderprogram)
	{
		shaderprogram = newShaderProgram;
		glUseProgram(shaderprogram);
	}

	glClearColor(1.0, 1.0, 1.0, 1.0); // set background colour
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	

	// Create perspective projection matrix
	glm::mat4 projection = glm::perspective(45.0f, 4.0f / 3.0f, 1.0f, 100.f);

	glm::mat4 identity(1.0); 

	if(!(rotation_x >= 89 && rotationDirection_x > 0) && !(rotation_x <= -89 && rotationDirection_x < 0))
	{
		rotation_x += rotationDirection_x;
	}
	if(!(rotation_y >= 89 && rotationDirection_y > 0) && !(rotation_y <= -89 && rotationDirection_y < 0))
	{
		rotation_y += rotationDirection_y;
	}

	// Apply model view transformations
	glm::mat4 modelview = glm::translate(identity,glm::vec3(0.0f, 0.0f, -2.5f));
	modelview = glm::rotate( modelview, rotation_x, glm::vec3(0.0f,1.0f,0.0f));
	modelview = glm::rotate( modelview, rotation_y, glm::vec3(1.0f,0.0f,0.0f));

	//pass modelview as uniform into shader
	GLuint modelviewIndex = glGetUniformLocation(shaderprogram, "modelview");
	glUniformMatrix4fv(modelviewIndex, 1, GL_FALSE, glm::value_ptr(modelview));

	//pass projection as uniform into shader
	GLuint projectionIndex = glGetUniformLocation(shaderprogram, "projection");
	glUniformMatrix4fv(projectionIndex, 1, GL_FALSE, glm::value_ptr(projection)); 

	//pass user defined parametters for the parallax occlusion mapping
	GLuint uniformIndex = glGetUniformLocation(shaderprogram, "heightMapScal");
	glUniform1f(uniformIndex, heightMapScal);
	uniformIndex = glGetUniformLocation(shaderprogram, "minSamples");
	glUniform1i(uniformIndex, minSamples);
	uniformIndex = glGetUniformLocation(shaderprogram, "maxSamples");
	glUniform1i(uniformIndex, maxSamples);

	//pass depth sampler used for the parallax occlusion mappings
	uniformIndex = glGetUniformLocation(shaderprogram, "textureDepth");
	glUniform1i(uniformIndex, 1);
	// set tex sampler to texture unit 0, arbitrarily
	uniformIndex = glGetUniformLocation(shaderprogram, "textureUnit0");
	glUniform1i(uniformIndex, 0);

	//bind the texture
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texID[indexCurrentHeightMap*2+1]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID[indexCurrentTexture*2]);

	//draw
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[4]);
	glDrawElements(GL_TRIANGLES, numSquareFaces * 3, GL_UNSIGNED_INT, 0);

	////////////////
	//Drawing labels
	////////////////

	if(shaderprogram != shaderprogramTextureSimple)
	{
		shaderprogram = shaderprogramTextureSimple;
		glUseProgram(shaderprogram);
	}

	//get the number of millisecond per frame
	currentTime = clock();
    float milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);

	std::stringstream strStream;
    strStream << "ms/frame: " << milliSecondsPerFrame;
	timeLabel.textToTexture(strStream.str().c_str(),textFont);
    timeLabel.draw(-0.8f,0.57f,projection,shaderprogram);
    lastTime = clock();

	heightMapScalLabel.draw(-0.8f,0.52f,projection,shaderprogram);
	minSamplesLabel.draw(-0.8f,0.47f,projection,shaderprogram);
	maxSamplesLabel.draw(-0.8f,0.42f,projection,shaderprogram);

	SDL_GL_SwapWindow(window);// swap buffers
}

void init(void)
{
	// loading all shaders that can be used;
	shaderprogramTextureSimple = initShaders("texture.vert", "texture.frag");
	shaderprogramPOM = initShaders("parallaxOcclusionMapping.vert", "parallaxOcclusionMapping.frag");

    glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object

    glGenBuffers(5, vbo); // Allocate two Vertex Buffer Objects (VBO)

    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // Bind 1st VBO as active buffer object
    // Copy the vertex data from diamond to the VBO
    // numSquareVerts * 3 * sizeof(GLfloat) is the size of the square array
    glBufferData(GL_ARRAY_BUFFER, numSquareVerts * 3 * sizeof(GLfloat), SquareVerts, GL_STATIC_DRAW);
    // Position data is going into attribute index 0 & has 3 floats per vertex
    glVertexAttribPointer(ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(ATTRIB_POSITION);     // Enable attribute index 0 (position)
 
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind 2nd VBO as active buf obj
    // numSquareVerts * 3 * sizeof(GLfloat) is the size of the normal array
    glBufferData(GL_ARRAY_BUFFER, numSquareVerts * 3 * sizeof(GLfloat), SquareVertNorms, GL_STATIC_DRAW);
    // normal data is going into attribute index 2 & has 3 floats per vertex 
    glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(ATTRIB_NORMAL);    // Enable attribute index 2 (normal)

	 glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); // bind 3nd VBO as active buf obj 
    // numSquareVerts * 2 * sizeof(GLfloat) is the size of the texture coordinate array
    glBufferData(GL_ARRAY_BUFFER, numSquareVerts * 2 * sizeof(GLfloat), SquareVertTex, GL_STATIC_DRAW);
    // texture Coordinate data is going into attribute index 3 & has 2 floats per vertex 
    glVertexAttribPointer(ATTRIB_TEXTURE_COORDINATE, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(ATTRIB_TEXTURE_COORDINATE);    // Enable attribute index 3 (texture Coordinate)

	glBindBuffer(GL_ARRAY_BUFFER, vbo[3]); // bind 4nd VBO as active buf obj
    // numSquareVerts * 3 * sizeof(GLfloat) is the size of the tangent array
    glBufferData(GL_ARRAY_BUFFER, numSquareVerts * 3 * sizeof(GLfloat), SquareVertTangents, GL_STATIC_DRAW);
    // tangent data is going into attribute index 4 & has 3 floats per vertex 
    glVertexAttribPointer(ATTRIB_TANGENT, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(ATTRIB_TANGENT);    // Enable attribute index 4 (tangent)

	/* glBindBuffer(GL_ARRAY_BUFFER, vbo[4]); // bind 5nd VBO as active buf obj 
    // numSquareVerts * 2 * sizeof(GLfloat) is the size of the binormal array
    glBufferData(GL_ARRAY_BUFFER, numSquareVerts * 3 * sizeof(GLfloat), SquareVertBinorms, GL_STATIC_DRAW);
    // binormal data is going into attribute index 5 & has 3 floats per vertex 
    glVertexAttribPointer(ATTRIB_BINORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(ATTRIB_BINORMAL);    // Enable attribute index 5 (binormal)*/

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[4]);// bind the 6nd vbo
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numSquareFaces * 3 * sizeof(GLuint), SquareFaces,	GL_STATIC_DRAW);//store the faces data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//loadingTexture

	texID = new GLuint[max(numberTexture,numberHeightMap)*2];

	int i = 0;
	for(int i = 0; i < max(numberTexture,numberHeightMap); i++)
	{
		if(i < numberTexture)
		{
			loadTexture(textureNames[i], &texID[i*2]);
		}
		if(i < numberHeightMap)
		{
			loadTexture(HeightMapNames[i], &texID[i*2+1]);
		}
	}


	//initializing the font
	if (TTF_Init()== -1)
		exitFatalError("TTF failed to initialise.");

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		exitFatalError("Failed to open font.");

	//create labels
	timeLabel = LabelClass();

	heightMapScalLabel = LabelClass();
	std::stringstream strStream;
	strStream << "height scale : " << heightMapScal;
	heightMapScalLabel.textToTexture(strStream.str().c_str(),textFont);

	std::stringstream strStream2;
	minSamplesLabel = LabelClass();
	strStream2 << "min Sample : " << minSamples;
	minSamplesLabel.textToTexture(strStream2.str().c_str(),textFont);

	std::stringstream strStream3;
	maxSamplesLabel = LabelClass();
	strStream3 << "max Sample : " << maxSamples;
	maxSamplesLabel.textToTexture(strStream3.str().c_str(),textFont);

   glEnable(GL_DEPTH_TEST); // enable depth testing
}

void cleanup(void) {
    glUseProgram(0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
	// could also detach shaders
    glDeleteProgram(shaderprogramTextureSimple);
	glDeleteProgram(shaderprogramPOM);
    glDeleteBuffers(5, vbo);
    glDeleteVertexArrays(1, &vao);
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[])
{
	cout << "-----------------------------------------------------------------" << endl
		<< "Controls for the demo :" << endl
		<< "-----------------------------------------------------------------" << endl
		<< " - Arrow key : rotating the square" << endl
		<< " - r : reset rotation to 0,0"<< endl
		<< " - p : toggle Parralax Occlusion Mapping"<< endl
		<< " - t : change texture"<< endl
		<< " - h : change heightMap"<< endl
		<< " - a : increase height scale; shift + a : decrease height scale" <<endl
		<< " - s : increase min Sample; shift + s : decrease min Sample" <<endl
		<< " - d : increase max Sample; shift + d : decrease max Sample" <<endl
		<< "-----------------------------------------------------------------" << endl
		<< "Other Information :" << endl
		<< "-----------------------------------------------------------------" << endl
		<< "Number of textures : " << numberTexture << endl
		<< "Number of heightMap : " << numberHeightMap << endl
		<< "-----------------------------------------------------------------" << endl<< endl;

    SDL_GLContext glContext; // OpenGL context handle
    SDL_Window * hWindow; // window handle
 
    hWindow = setupRC(glContext);

	GLenum err = glewInit();
	// Required on Windows... init GLEW to access OpenGL beyond 1.1
	// remove on other platforms
	if (GLEW_OK != err)
	{	// glewInit failed, something is seriously wrong.
		exitFatalError("glewInit failed, aborting.");
	}

	init();

    SDL_Event sdlEvent;	// variable to detect SDL events

	bool running = true;
	while (running)		// the event loop
	{
		SDL_PollEvent(&sdlEvent);
		running = handleSDLEvent(sdlEvent);
		//update();			// not used yet!
		draw(hWindow);

	}

	cleanup();

	SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}

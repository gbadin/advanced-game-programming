#include "LabelClass.h"

#include <sstream>
#include <string>
#include <iostream>
#include <GL/glew.h>
#include <SDL_ttf.h>
#include <SDL.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

using namespace std;

LabelClass::LabelClass(void)
{
}

LabelClass::~LabelClass()
{
	glDeleteTextures(1, &texID);
}

GLuint LabelClass::textToTexture(const char * str, TTF_Font* textFont)
{
	SDL_Color colour = {0,0,0};
	return commonTextToTexture(str, textFont, colour);
}

GLuint LabelClass::textToTexture(const char * str, TTF_Font* textFont, SDL_Color colour)
{
	return commonTextToTexture(str, textFont, colour);
}

GLuint LabelClass::commonTextToTexture(const char * str, TTF_Font* textFont, SDL_Color colour)
{
	glDeleteTextures(1, &texID);
	glDeleteBuffers(3, vbo);
	glDeleteVertexArrays(1, &vao);
	SDL_Surface *stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)
	{
		cout << "String surface not created.  " << endl;
		cout << SDL_GetError();
		SDL_Quit();
		cin.get();
		exit(1);
	}

	width = stringImage->w;
	height = stringImage->h;
	glGenTextures(1, &texID);

	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	glBindTexture(GL_TEXTURE_2D, texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

	SDL_FreeSurface(stringImage);

	float vertices [][3] = {
		{0.0f,0.0f,0.0f},
		{width*0.0015f,0.0f,0.0f},
		{0.0f,height*0.0015f,0.0f},
		{width*0.0015f,height*0.0015f,0.0f}
	};

	float textCoord [][2] = {
		{0.0f,1.0f},
		{1.0f,1.0f},
		{0.0f,0.0f},
		{1.0f,0.0f}
	};

	int faces [][3] = {
		{0,1,2},
		{1,2,3},
	};

	glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object

    glGenBuffers(3, vbo); // Allocate two Vertex Buffer Objects (VBO)

    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // Bind 1st VBO as active buffer object
    // Copy the vertex data from diamond to the VBO
    // 4 * 3 * sizeof(GLfloat) is the size of the square array
    glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
    // Position data is going into attribute index 0 & has 3 floats per vertex
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);     // Enable attribute index 0 (position)

	 glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind 2nd VBO as active buf obj 
    // 4 * 2 * sizeof(GLfloat) is the size of the texture coordinate array
    glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), textCoord, GL_STATIC_DRAW);
    // texture Coordinate data is going into attribute index 3 & has 2 floats per vertex 
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(3);    // Enable attribute index 3 (texture Coordinate)

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[2]);// bind the 3nd vbo
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * 3 * sizeof(GLuint), faces,GL_STATIC_DRAW);//store the faces data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return texID;
}

void LabelClass::draw (float x, float y,glm::mat4 projection,GLuint shaderprogram)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4 identity(1.0); 

	// Apply model view transformations
	glm::mat4 modelview = glm::translate(identity,glm::vec3(x, y, -1.5f));

	//pass modelview as uniform into shader
	int modelviewIndex = glGetUniformLocation(shaderprogram, "modelview");
	glUniformMatrix4fv(modelviewIndex, 1, GL_FALSE, glm::value_ptr(modelview));

	//pass projection as uniform into shader
	int projectionIndex = glGetUniformLocation(shaderprogram, "projection");
	glUniformMatrix4fv(projectionIndex, 1, GL_FALSE, glm::value_ptr(projection));

	// set tex sampler to texture unit 0, arbitrarily
	GLuint uniformIndex = glGetUniformLocation(shaderprogram, "textureUnit0");
	glUniform1i(uniformIndex, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[2]);
	glDrawElements(GL_TRIANGLES, 2 * 3, GL_UNSIGNED_INT, 0);
}
#ifndef LABEL_H
#define LABEL_H

#include <GL/glew.h>
#include <SDL_ttf.h>
#include <SDL.h>
#include <glm\glm.hpp>

class LabelClass
{
public:
	LabelClass(void);
	GLuint textToTexture(const char * str, TTF_Font* textFont);
	GLuint textToTexture(const char * str, TTF_Font* textFont,  SDL_Color colour);
	void draw (float x, float y,glm::mat4 projection,GLuint shaderprogram);
	~LabelClass();
private :
	GLuint texID;
	GLuint height;
	GLuint width;
	GLuint vao;
	GLuint vbo[3];
	GLuint LabelClass::commonTextToTexture(const char * str, TTF_Font* textFont, SDL_Color colour);
};

#endif
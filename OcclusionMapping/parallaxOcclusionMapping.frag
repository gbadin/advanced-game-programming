//parralax occlusion mapping fragment shader, inspired by an implementation of POM in HLSL on the site gamedev.net and from the windows directX DK
#version 130
// OpenGL 3.2 replace above with #version 150
// Some drivers require the following
precision highp float;

in  vec3 ex_NormalWS;
in  vec3 ex_eyeTS;
in  vec3 ex_eyeWS;
in vec2 ex_TexCoord;

uniform float heightMapScal;
uniform int minSamples;
uniform int maxSamples;

uniform sampler2D textureUnit0;
uniform sampler2D textureDepth;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor

void main(void)
{
	vec3 eyeTS = ex_eyeTS;
	eyeTS.y = -ex_eyeTS.y;// why ???????

	float parallaxLimit = length(eyeTS.xy) / eyeTS.z;// calculate the parallax offset vector max length
	parallaxLimit *= heightMapScal;//scale the vector according to height-map scale (heightMapScal is a float that determine the intensity of the effect)
	
	vec2 parallaxOffset = normalize(-eyeTS.xy);// calculate the parallax offset vector direction
	parallaxOffset *= parallaxLimit;// calculate the final maximum offset vector

	int numMaxSample = int(mix(maxSamples, minSamples, dot(ex_eyeWS, ex_NormalWS)));// calculate dynamic number of samples

	float stepSize = 1.0/float(numMaxSample);// calculate the texcoord step size

	vec2 dx = dFdx(ex_TexCoord);// calculate the texcoord partial derivative in x in screen space for texturegrad
	vec2 dy = dFdy(ex_TexCoord);// calculate the texcoord partial derivative in y in screen space for texturegrad

	vec2 offsetStep = stepSize * parallaxOffset;
	vec2 currOffset = vec2(0,0);
	vec2 lastOffset = vec2(0,0);

	float currDepthSample;
	float lastDepthSample;

	float stepHeight = 1.0;
	int indexCurrSample = 0;

	while(indexCurrSample < numMaxSample)
	{
	   currDepthSample = textureGrad(textureDepth, ex_TexCoord + currOffset, dx, dy).x;// sample the current texcoord offset
	   if(currDepthSample > stepHeight)
	   {
			//calculate the linear intersection point
			float ua = (lastDepthSample - (stepHeight + stepSize)) / (stepSize + (currDepthSample - lastDepthSample));
			currOffset = lastOffset + ua * offsetStep;

			indexCurrSample = numMaxSample + 1;
	   }
	   else
	   {
			indexCurrSample ++;
			stepHeight -= stepSize;
			lastOffset = currOffset;
			currOffset += offsetStep;
			lastDepthSample = currDepthSample;
	   }
	}
	
	gl_FragColor = textureGrad(textureUnit0, ex_TexCoord + currOffset, dx, dy);
}
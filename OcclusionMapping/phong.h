#ifndef PHONG
#define PHONG

// structs for lights and materials
// include alpha component - allowing for transparent surfaces
struct lightStruct
{
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float position[4];
};

struct materialStruct
{
	float ambient[4];
	float diffuse[4];
	float specular[4];
	//vec4 colour;
	float shininess;
};

#endif
//parralax occlusion mapping vertex shader, inspired by an implementation of POM in HLSL on the site gamedev.net and from the windows directX DK
#version 130
// OpenGL 3.2 replace above with #version 150

uniform mat4x4 projection;
uniform mat4x4 modelview;

in  vec3 in_Position;
in  vec3 in_Normal;
in  vec3 in_Tangent;
in	vec2 in_TexCoord;

out vec3 ex_NormalWS;
out vec3 ex_eyeTS;
out vec3 ex_eyeWS;
out vec2 ex_TexCoord;

void main(void)
{
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	// The per-vertex tangent, binormal, normal form the tangent to object space
	// rotation matrix. Multiply by the world matrix to form tangent to world space
	// rotation matrix. Then transpose the matrix to form the inverse tangent to
	// world space, otherwise called the world to tangent space rotation matrix.

	vec3 normal = normalize(modelview * vec4(in_Normal, 1.0)).xyz;
	vec3 tangent = normalize(modelview * vec4(in_Tangent, 1.0)).xyz;
	vec3 binormal = cross(normal,tangent);

	mat3 worldToTangentSpace = mat3(tangent,binormal,normal);

	gl_Position = projection * vertexPosition;
	ex_TexCoord = in_TexCoord;

	ex_eyeTS = worldToTangentSpace * -vertexPosition.xyz;
	ex_eyeWS = normalize(-vertexPosition.xyz);
	ex_NormalWS = normal;
}